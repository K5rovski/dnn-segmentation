from keras.models import Sequential,Model, load_model
from keras.layers import Convolution2D, Conv2D,MaxPooling2D,Input,Cropping2D,merge,Lambda, \
            Dense,Dropout,BatchNormalization,Activation, Flatten, Reshape, UpSampling2D, ZeroPadding2D
from keras.optimizers import SGD
from keras import initializers
# from keras.utils.visualize_util import  plot
import numpy as np
import tensorflow as tf
import sys

sys.path.append('../../..')

from .AutoEncoderLayer import AutoEncoderLayer
from .CRBMPretrained import CRBMPretrained


def strez_init(mean, stdev,**kwargs):
    return initializers.RandomNormal(mean, stdev)

def get_2k_image_pretrained(img_w,img_h,added_stuff=None,ch_add=3):

    x = Input(shape=(img_w + (8 - (img_w % 8)),
                     img_h + (8 - (img_h % 8)), ch_add))
    #
    if added_stuff['use_crbm']:
        x = CRBMPretrained((42, 42, 80), added_stuff['path_pretrain'], input_shape=(img_w,img_h,3),
                                )(x)
        x = Activation('sigmoid')(x)


    elif  added_stuff['use_autoenc']:
        x = BatchNormalization()(x)
        x = added_stuff['layer'](x)
        x = Activation('relu')(x)
        x = BatchNormalization()(x)
    else:
        x = BatchNormalization()(x)
        x = Convolution2D(80, 4, 4, init='glorot_normal', activation='linear', border_mode='same')(x)
        x = Activation('relu')(x)
        
        
    x = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), border_mode='same')(x)

    x = Convolution2D(70, 4, 4, init='glorot_normal', activation='linear', border_mode='same')(x)
    x = Activation('relu')(x)

    x = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), border_mode='same')(x)
    x = Dropout(0.25)(x)

    x = Convolution2D(60, 4, 4, init='glorot_normal', activation='linear', border_mode='same')(x)

    x = Activation('relu')(x)
    x = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), border_mode='same', )(x)

    x = Dropout(0.25)(x)

    x = Flatten()(x)


    x = Dense(500)(x)
    x = Activation('relu')(x)
    x = Dropout(0.5)(x)

    model = Dense(3, init='glorot_normal', activation='softmax')(x)

    print(model.summary())

    return model



def pad_one(x):
    # x_s=tf.shape(x)
    new_x=tf.pad(  x, [[0,0],[0,1],[0,1],[0,0]] )
    # new_x=tf.transpose(new_x,[0,3,1,2])
    return new_x

def get_autoencoded(imgw,imgh):
    input_img = Input(shape=(imgw + (8 - (imgw % 8)),
                             imgh + (8 - (imgh % 8)), 3))  # adapt this if using `channels_first` image data format

    # pad_img=Lambda(pad_one,  )(input_img)
    pad_img=BatchNormalization()(input_img)

    x = Conv2D(80, (4, 4), activation='relu',padding='same')(pad_img)
    x=Dropout(0.5)(x)
    x = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), padding='same')(x)

    x = Conv2D(60, (4, 4), activation='relu', padding='same')(x)
    x = Dropout(0.33)(x)
    x = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), padding='same')(x)
    #
    x = Conv2D(40, (4, 4), activation='relu', padding='same')(x)
    x = Dropout(0.25)(x)
    x = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), padding='same')(x)

    encoded=x
    # at this point the representation is (4, 4, 8) i.e. 128-dimensional

    x = Conv2D(40, (4, 4), activation='relu', padding='same')(encoded)
    x = Dropout(0.25)(x)
    x = UpSampling2D((2, 2))(x)

    x = Conv2D(60, (4, 4), activation='relu', padding='same')(x)
    x = Dropout(0.33)(x)
    x = UpSampling2D((2, 2))(x)
    #
    x = Conv2D(80, (4, 4), activation='relu', padding='same')(x)
    x = Dropout(0.5)(x)
    x = UpSampling2D((2, 2))(x)


    decoded = Conv2D(3, (4, 4), activation='sigmoid', padding='same')(x)

    model = Model(input_img, decoded)
    model.compile(optimizer='adadelta', loss='binary_crossentropy')
    return model



