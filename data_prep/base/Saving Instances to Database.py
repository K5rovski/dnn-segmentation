#!/usr/bin/env python
# coding: utf-8

# # Saving Training Instances
# 
# 
# The instances whic
# 
# This module deals with saving all training/validation instances -- for ease of convinience.
# 
# Using different options:
# + image masks -- Training instances;
# + uniform sampling parameters -- Validation Instances.
# 
# **Image patches** (used as input/output for the neural networks). are saved to disk 
# 
# + Instances are automatically saved in a **random**, **batched** way. -- For running the experiment, using a smaller sized RAM Memory

# ---------

# ## Importing Modules
# 
# Here we import the needed python functions for sampling ROIs.
# 
# + The database used was [LMDB](https://lmdb.readthedocs.io/).
# + The data are saved in a key-value pair base, with randomly assigned keys.
# 

# In[1]:


import glob
import os
import time
import pickle
import numpy as np
import sys
from os.path import join


sys.path.append('../../..')
import dnn_seg

#SETUPP    ============================
from  dnn_seg.data_prep.utils.create_lmdb_batch_funcs import           reduce_files,make_db_folder,create_lmdbs

from  dnn_seg.data_prep.utils.data_helper import create_chessmask

from dnn_seg.net.utils.models import get_2k_image_2layer_convnetmodel
from dnn_seg.net.utils.train_h import save_relevant

from keras.optimizers import SGD

print('Every import is succesful !')


# ## Saving Configutration Experiment Files
# 
# This code block is used to save the database step -- configuration text files.
# 
# **For experiment recreation, this saved configuration text file has all needed info to reproduce the same output.**
# 

# In[2]:




np.random.seed(None)
quick_str='base_'+''.join(map(chr,np.random.randint(97,97+26,(5,))) )
files_save=[]

conf_test_tosave=save_relevant('../data/saved_base_confs',quick_str,
            files=files_save,
            just_return=True)

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


# ## Setting parameters
# 
# 
# 
# 

# In[11]:




DESCRIPTION_TEXT='normal sample, DNN view single patch'

db_folder='/home/monster/datasets/BASES/basic_15mil_randomshuffle_imgs_dnnset'
BASE_IMG_FOLDER = '/home/monster/datasets/mlsets01-02'

save_base=True
image_width, image_height = 45, 45
big_image_size=2048


lookup_path = join(BASE_IMG_FOLDER, 'interim')

groundtruth_path = join(BASE_IMG_FOLDER, 'corrected')


# ## Randomly shuffling the ml images

# In[12]:


import random 

core_name = lambda img: img[img.rindex('sp'):img.rindex('img')+5]

random.seed(7777)

sort_list = sorted(os.listdir(groundtruth_path))
train_factor = 0.77 or 0
train_stubs = []

val_factor = 0.15 or 0
val_stubs = []

train_images = []
val_images = []

for img in sort_list:
    shuffle = random.random()
    
    if img in train_stubs or shuffle<train_factor:
        train_images.append(core_name(img))
        
    elif img in val_stubs or (shuffle>=train_factor and shuffle<(train_factor+val_factor)):
        val_images.append(core_name(img))


print(train_images, val_images, len(train_images), len(val_images))


# ## Last Configs

# In[13]:





print('validating using these imgs: ', val_images)


test_image_val=[filename for ind,filename in enumerate(glob.glob(os.path.join(lookup_path, '*.tif')) ) if  filename[filename.rindex(os.path.sep)+1:filename.rindex('img')+5] in val_images ]

val_do_expand=False
percent_patches_used_val=np.array([1,1,1])*0.1  #np.array([0.837,0.744,1])*0.1 #[4.0/19,7.0/10,1]
phase_done_val='val'
val_batch_size=1*10**5
val_lmdb_GBsize=200

assert len(test_image_val) == len(val_images)


# In[14]:


'''Train Parameters'''

train_mask_dir = join(BASE_IMG_FOLDER, 'training-instances')


print('training using these imgs: ', train_images)




test_image_train=[filename for ind,filename in enumerate(glob.glob(os.path.join(lookup_path, '*.tif')) )
 if  filename[filename.rindex(os.path.sep)+1:filename.rindex('img')+5] in train_images ]

train_do_expand=False
percent_patches_used_train= np.array([1,1,1])*0.65#*(1.0/8)  #[4.0/19,7.0/10,1]
phase_done_train='train'

train_batch_size=1*10**5
train_lmdb_GBsize=150


# AEN Train Flow

# do_special = {'perc_switch':0.8,'two_patch':True}


# DNN Train Flow
do_special={'perc_switch':0,'not_mix_patches':True,
            'two_patch':True,'folded_expand':True}

assert len(test_image_train) == len(train_images)


# ### Not used parameters 
# 
# -----

# In[15]:



#!!!!!!!!!!!!NOT NEEDED ----------------------------------------------------------------------


image_list_folder='#########'
image_prepend='###########'
save_path = '#################'
patch_dir='##################'
save_path = '#################'
# =============================================



# ## Running the database saving process
# 
# The two(train and val) image sets are iterated,
# and saved in a **batched** way.
# 
# 
# 

# In[ ]:




# -----------------------


# =====================================================================
start_time = time.time()
print ('Creating images at "%s" ...' % db_folder)
print( 'Working on: ',len(test_image_val),test_image_val)

make_db_folder(db_folder)

create_lmdbs(db_folder,    phase_done_val,   
             (test_image_val,lookup_path, groundtruth_path, 
                  "all_test", 0, save_path),
             image_list_folder,image_prepend,image_width,
             image_height,smaller_size=percent_patches_used_val,
             random_key_prepend=12,do_expand=val_do_expand,
             patch_size=image_width,save_base=save_base,
             batch_size=val_batch_size,base_GBsize=val_lmdb_GBsize,
            do_special=do_special
)
reduce_files(db_folder,phase_done_val)

print( 'Done after {:.2f} hours'.format((time.time() - start_time)/3600 ))


start_time = time.time()

print ('Working on: ',len(test_image_train),test_image_train)

create_lmdbs(db_folder,    phase_done_train,   
             (test_image_train,lookup_path, groundtruth_path,
                      "all_test", 0, save_path)
             ,image_list_folder,image_prepend,image_width,
             image_height,smaller_size=percent_patches_used_train,
             mean_name='mean.jpg',random_key_prepend=12,
             do_expand=train_do_expand,patch_size=image_width,
             save_base=save_base,batch_size=train_batch_size,
             mask_dir=train_mask_dir,base_GBsize=train_lmdb_GBsize,
           do_special=do_special
            )
reduce_files(db_folder,phase_done_train)

print( 'Done after {:.2f} hours'.format((time.time() - start_time)/3600 ))

# ====================================================================


# ## Actually saving the configuration info

# In[ ]:




# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

save_relevant('data_funcs/saved_baseconfs',quick_str,str_to_save=conf_test_tosave,descriptive_text=DESCRIPTION_TEXT)

