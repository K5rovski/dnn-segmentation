# ----------------------------------------------------------
#
# Run inside imagej
#
# ----------------------------------------------------------


from ij import IJ 
from ij.io import FileSaver 
import os
from os.path import join, split
from ij.plugin.frame import RoiManager
from ij.gui import Roi
from ij.measure import ResultsTable
from ij.plugin.filter import ParticleAnalyzer
from ij.plugin.filter.ParticleAnalyzer import ADD_TO_MANAGER,EXCLUDE_EDGE_PARTICLES
import time
import random
core_name = lambda img: img[img.rindex('sp'):img.rindex('img')+5]

random.seed(7777)


lookup_path=r"/home/monster/datasets/mlsets01-02/interim"
saving_dir=r"/home/monster/datasets/mlsets01-02/roi-big-cells/"

doFileSave=True
doRmClose=False
leaveMasks=False

train_images = ['sp13726-img04', 'sp13726-img05', 'sp13726-img06', 'sp13750-img01', 'sp13750-img02', 
'sp13750-img03', 'sp13750-img04', 'sp13750-img07', 'sp13878-img05', 'sp13878-img06', 'sp13878-img07',
'sp13878-img08', 'sp13909-img01', 'sp13909-img02', 'sp13909-img03', 'sp13909-img04', 'sp13909-img07',
'sp13909-img08', 'sp13933-img01', 'sp13933-img02', 'sp13933-img03','sp13726-img02', 'sp13726-img03', 
'sp13933-img04', 'sp13933-img05', 'sp13933-img06', 'sp13933-img08', 'sp13938-img03', 'sp13938-img07',
'sp14105-img02', 'sp14105-img06', 'sp14105-img07', 'sp14252-img01', 
'sp14252-img02', 'sp14252-img03', 'sp14252-img05', 'sp14252-img06', 'sp14252-img07', 'sp14252-img08',
'sp14436-img01', 'sp14436-img02', 'sp14436-img03', 'sp14436-img04', 'sp14436-img05', 'sp14436-img08']


train_factor =  0

sorted_file_list = sorted(os.listdir(lookup_path))
	   
proc_images=[os.path.join(lookup_path,filename) for ind,filename in enumerate(sorted_file_list )\
	if  core_name(split(filename)[1]) in train_images or random.random()<train_factor  ]

print('Training using {} imgs'.format( len(proc_images)))

# ----------------------------------------------------------


if not os.path.exists(saving_dir):
	os.makedirs(saving_dir)
	print('i made it')
else:
	print('it already exists')



rm=None

for img_ind,image in enumerate(proc_images):
	imp = IJ.openImage(image);
	imp.show()

	IJ.setThreshold(128,128)

	#rm = None#RoiManager.getInstance()
	if not rm:
	  rm = RoiManager()
	else:
		rm.reset()
	
	#IJ.run(imp,"Analyze Particles...", "size=112-Infinity exclude add")
	rt=ResultsTable()
	pa=ParticleAnalyzer(EXCLUDE_EDGE_PARTICLES | ADD_TO_MANAGER,
                        0,
                        rt,
                        1600,
                        float('inf'),
                        0.0,
                        1.0)

	pa.analyze(imp)
	# time.sleep(2)
	print 'Size of results: ',rt.size()
	# rm.runCommand("select","all")
	# rm.runCommand("Fill","3")
	save_path=saving_dir+"/mask_%s" % ( split(image)[1] )
	# print(save_path)
	impMask = IJ.createImage("Mask", "8-bit grayscale-mode", imp.getWidth(), imp.getHeight(), imp.getNChannels(), imp.getNSlices(), imp.getNFrames())
	impMask.show()
	IJ.setForegroundColor(255, 255, 255)
	
	rm.runCommand(impMask,"Deselect")
	rm.runCommand(impMask,"Draw")
	

	if doFileSave and FileSaver(impMask).saveAsTiff(save_path):
		print 'Saved Image ',image
	else:
		print '!!! Not saved Image',image
	
	if not leaveMasks and img_ind<len(proc_images)-1:
		impMask.changes=False
		impMask.close()	
		imp.changes=False
		imp.close()
	
	# rm.runCommand("save selected", save_path)
if doRmClose: rm.close()